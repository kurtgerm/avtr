import React, { Component } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
} from 'react-native';

class Input extends Component {
  render() {
    if (this.props.hide) {
      return null;
    }

    return (
      <View style={styles.container}>
        <TextInput
          onChangeText={this.props.onChangeAvatarText}
          style={styles.textInput}
          placeholder="Type Something"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  textInput: {
    backgroundColor: '#F1F3F6',
    padding: 10,
    textAlign: 'center',
  }
});

export default Input;