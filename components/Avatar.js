import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';

class Avatar extends Component {
  render() {
    if (this.props.avatarText.length > 0) {
      return (
        <View style={[styles.container, this.props.fullscreen ? styles.fullscreenContainer : null]}>
          <TouchableOpacity onPress={this.props.onPress}>
            <Image
              style={styles.avatarImage}
              source={{ uri: 'https://api.adorable.io/avatars/230/' + this.props.avatarText }}
            />
          </TouchableOpacity>
        </View>
      );
    }

    return (
      <View style={[styles.container, this.props.fullscreen ? styles.fullscreenContainer : null]}>
        <TouchableOpacity onPress={this.props.onPress}>
          <View style={styles.placeholder} />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  fullscreenContainer: {
    flex: 1,
  },
  avatarImage: {
    width: 230,
    height: 230,
    borderRadius: 115,
  },
  placeholder: {
    backgroundColor: '#F1F3F6',
    width: 230,
    height: 230,
    borderRadius: 115,
  }
});

export default Avatar;