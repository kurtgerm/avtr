import React from 'react';
import {
  LayoutAnimation,
  Text,
  TextInput,
  SafeAreaView,
  View,
} from 'react-native';

import Avatar from './components/Avatar';
import Input from './components/Input';

export default class App extends React.Component {
  state = {
    avatarText: '',
    isViewingAvatar: false,
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        <Avatar
          avatarText={this.state.avatarText}
          fullscreen={this.state.isViewingAvatar}
          onPress={this.toggleIsViewingAvatar}
        />
        <Input
          onChangeAvatarText={this.onChangeAvatarText}
          hide={this.state.isViewingAvatar}
        />
      </SafeAreaView>
    );
  }

  onChangeAvatarText = (avatarText) => {
    this.setState({ avatarText });
  }

  toggleIsViewingAvatar = () => {
    LayoutAnimation.easeInEaseOut();

    this.setState({
      isViewingAvatar: !this.state.isViewingAvatar
    });
  }
}
